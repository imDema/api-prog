#define HASH_A 33
#define HASH_B 101

const int primes_size = 72;
const int primes[] = {
            3, 7, 11, 17, 23, 29, 37, 47, 59, 71, 89, 107, 131, 163, 197, 239, 293, 353, 431, 521, 631, 761, 919,
            1103, 1327, 1597, 1931, 2333, 2801, 3371, 4049, 4861, 5839, 7013, 8419, 10103, 12143, 14591,
            17519, 21023, 25229, 30293, 36353, 43627, 52361, 62851, 75431, 90523, 108631, 130363, 156437,
            187751, 225307, 270371, 324449, 389357, 467237, 560689, 672827, 807403, 968897, 1162687, 1395263,
            1674319, 2009191, 2411033, 2893249, 3471899, 4166287, 4999559, 5999471, 7199369 };

#define LOADFAC 0.74f
#define DEFAULT_DIRECT_HT_SIZE 3

uint hash(const char* word)
{
    uint h = 5381;
    for(int i = 0; word[i]; i++)
    {
        h = (h*HASH_A) + word[i]; //ALLOW OVERFLOW
    }
    return h;
}

typedef struct bucket //16 bytes each
{
    const char* key;
    void* value;
} bucket;

struct _direct_ht //24 bytes each
{
    int count;
    int size;
    int loadsize;
    int occupied;
    struct bucket* buckets;
};
typedef struct _direct_ht direct_ht;

bucket* new_buckets(int size)
{
    return calloc(size, sizeof(bucket));
}

int get_prime_size(const int approx_size)
{
    int size = __INT_MAX__;
    for(int s = 0; s < primes_size; s++)
        if(primes[s] >= approx_size)
        {
            size = primes[s];
            break;
        }
    return size;
}

direct_ht create_direct_ht(int min_size)
{

    int newsize = get_prime_size(min_size);
    struct _direct_ht new_ht;
    new_ht.count = 0;
    new_ht.size = newsize;
    new_ht.loadsize = newsize * LOADFAC;
    new_ht.occupied = 0;
    new_ht.buckets = new_buckets(newsize);
    return new_ht;
}

direct_ht* new_direct_ht(int min_size)
{
    direct_ht* ht = malloc(sizeof(struct _direct_ht));
    ht->count = 0;
    ht->size = 1;
    ht->loadsize = 1;
    ht->occupied = 0;
    ht->buckets = calloc(1,sizeof(bucket));
    return ht;
}

void ht_put(direct_ht* ht, const char* key, void* value, uint hash)
{
    //Starting index
    int index = hash % ht->size;
    //Increments
    uint h2 = 1 + ((unsigned long) hash * HASH_B) % (ht->size - 1);
    //Find an open slot
    while(1)
    {
        if(ht->buckets[index].key == NULL)
            break;
        index = (index + h2) % ht->size;
    }
    if(ht->buckets[index].value == NULL) //Increase occupation only if the bucket was never used
        ht->occupied++;
    ht->buckets[index].key = key;
    ht->buckets[index].value = value;
    ht->count++;
}

void resize_hashtable(direct_ht* ht, int size)
{
    direct_ht new_ht = create_direct_ht(size);

    for(int i = 0; i < ht->size; i++)
    {
        bucket bkt = ht->buckets[i];
        if(bkt.key != NULL)
        {
            ht_put(&new_ht, bkt.key, bkt.value, hash(bkt.key));
        }
    }

    free(ht->buckets);
    *ht = new_ht;
}

void* ht_search(const direct_ht* ht, const char* key, uint hash)
{
    if(ht->size == 1)
    {
        if(ht-> count == 1 && !strcmp(ht->buckets[0].key,key))
            return ht->buckets[0].value;
        else return NULL;
    }
    //Starting index
    int index = hash % ht->size;
    //Increments
    uint h2 = 1 + ((unsigned long) hash * HASH_B) % (ht->size - 1);
    
    while(1)
    {
        bucket bkt = ht->buckets[index];

        if(bkt.key != NULL && !strcmp(bkt.key, key)) //Found
            return bkt.value;

        else if(bkt.key == NULL && bkt.value != ht->buckets) //Landed on empty no collisions bucket
            return NULL;

        index = (index + h2) % ht->size;
    }
}

const char* ht_search_keyptr(const direct_ht* ht, const char* key, uint hash)
{
    if(ht->size == 1)
    {
        if(!strcmp(ht->buckets[0].key,key))
            return ht->buckets[0].key;
        else return NULL;
    }
    //Starting index
    int index = hash % ht->size;
    //Increments
    uint h2 = 1 + ((unsigned long) hash * HASH_B) % (ht->size - 1);
    
    while(1)
    {
        bucket bkt = ht->buckets[index];

        if(bkt.key != NULL && !strcmp(bkt.key, key)) //Found
            return bkt.key;

        else if(bkt.key == NULL && bkt.value != ht->buckets) //Landed on empty no collisions bucket
            return NULL;

        index = (index + h2) % ht->size;
    }
}

void ht_insert(direct_ht* ht, const char* key, void* value, uint hash)
{
    if(ht->size == 1)
    {
        if(ht->count == 0)
        {
            ht->count = 1;
            ht->buckets[0].key = key;
            ht->buckets[0].value = value;
            return;
        }
        else
            resize_hashtable(ht, DEFAULT_DIRECT_HT_SIZE);
    }
    else if(ht->occupied >= ht->loadsize )
    {
        if(ht->count > ht->occupied / 3)
            resize_hashtable(ht, ht->size * 2);
        else
            resize_hashtable(ht, ht->size);
    }
    ht_put(ht, key, value, hash);
}

void ht_delete(direct_ht* ht, const char* key, uint hash)
{
    if(ht->size == 1)
    {
        if(ht->count == 1 && !strcmp(ht->buckets[0].key,key))
        {
            ht->buckets[0].value = 0;
            ht->buckets[0].key = 0;
            ht->count = 0;
        }
        return;
    }
    //Starting index
    int index = hash % ht->size;
    //Increments
    uint h2 = 1 + ((unsigned long) hash * HASH_B) % (ht->size - 1);
    //Find an open slot
    while(1)
    {
        bucket bkt = ht->buckets[index];
        if(bkt.key != NULL && !strcmp(bkt.key, key)) //Found
        {
            //Free key, set hash to 0 and mark as deleted by setting value to ht->bucket
            ht->buckets[index].value = ht->buckets; //Marked as deleted to keep potential colliding walks intact
            ht->buckets[index].key = NULL;
            ht->count--;
            break;
        }
        else if(bkt.key == NULL && bkt.value != ht->buckets) //Landed on empty no collisions bucket, item didn't exist
            break;

        index = (index + h2) % ht->size;
    }
    //Decrease if many deleted //TODO CHECK THIS
    if(ht->count < 3)
        resize_hashtable(ht, 3);
    else if(ht->size > 3 && ht->count < ht->loadsize / 2 - 1)
    {
        resize_hashtable(ht, ht->size / 2);
    }
}

void ht_free(direct_ht* ht)
{
    if(ht == NULL)
        return;
    free(ht->buckets);
    free(ht);
}